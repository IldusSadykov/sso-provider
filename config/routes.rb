Rails.application.routes.draw do
  #devise_for :users, controllers: { registrations: "users/registrations" }
  root to: "pages#home"

  devise_for :users, sign_out_via: [:get, :delete],
    controllers: { omniauth_callbacks: 'callbacks' }

       # Provider stuff
  get '/auth/sso/authorize', to: 'auth#authorize_me'
  match '/auth/sso/access_token' => 'auth#access_token', via: :all
  match '/auth/sso/user' => 'auth#user', via: :all
  match '/oauth/token' => 'auth#access_token', via: :all
end
